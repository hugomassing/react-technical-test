import React from "react";
import { render } from "@testing-library/react";

// Components to tests
import { BasicButton } from "./Button.stories";

describe("Renders button component", () => {
  test("without crashing", () => {
    const { container } = render(<BasicButton />);
    expect(container).toBeTruthy();
  });
});
