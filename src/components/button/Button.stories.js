import React from "react";
import { action } from "@storybook/addon-actions";
import Button from "./Button";

export default {
  title: "Button",
  component: Button
  // includeStories: []
  // permet à Storybook de ne pas charger les stories de ce fichier.
  // Tu devras donc importer tes stories dans Button.stories.mdx pour qu'elles s'affichent
  //dans l'onglet 'Docs' de Storybook
};

// Basic button with only onClick props
export const BasicButton = () => (
  <Button onClick={action("clicked")}>Hello Button</Button>
);

// Button with a type "primary"
export const PrimaryButton = () => (
  <Button onClick={action("clicked")} type="primary">
    Primary Button
  </Button>
);

// Disabled Button
export const DisabledButton = () => (
  <Button onClick={action("clicked")} disabled>
    Disabled Button
  </Button>
);
