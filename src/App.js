import React from 'react';
import './App.css';
import Button from './components/button/Button';

const App = () => (
  <div className="App">
    <h1>Hello world</h1>
    <Button>Click me</Button>
  </div>
)

export default App;
